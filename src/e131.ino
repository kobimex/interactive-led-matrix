/*
 * Asynchronous E.131 (sACN) library for Arduino ESP8266 and ESP32
 * Copyright (c) 2019 Shelby Merrick
 * http://www.forkineye.com
 */

#include <WiFi.h>
#include <FastLED.h>
#include <ESPAsyncE131.h>
#include <PubSubClient.h>
#include <ArduinoOSC.h>

//numero di universo iniziale e numero totale di universi sACN
#define UNIVERSE 1
#define UNIVERSE_COUNT 2
#define NUM_LEDS 289

const char ssid[] = "";
const char passphrase[] = "";
const char mqtt_server[] = "mqtt.hacklabcormano.it";

WiFiClient espClient;
PubSubClient client(espClient);

OscWiFi osc;

ESPAsyncE131 e131(UNIVERSE_COUNT);

CRGB leds[NUM_LEDS];

void setup() {
        Serial.begin(115200);

        //wifi setup
        WiFi.mode(WIFI_STA);

        Serial.println("");
        Serial.print(F("Connecting to "));
        Serial.print(ssid);

        if (passphrase != NULL)
                WiFi.begin(ssid, passphrase);
        else
                WiFi.begin(ssid);

        while (WiFi.status() != WL_CONNECTED) {
                delay(500);
                Serial.print(".");
        }

        Serial.println("");
        Serial.print(F("Connected with IP: "));
        Serial.println(WiFi.localIP());

        client.setServer(mqtt_server, 1883);
        client.setCallback(callbackMQTT);

        //OSC setup
        osc.begin(8000);
        osc.subscribe("/touch1", [](OscMessage& m)
        {
                // parsing messaggio OSC
                int x = (mapfloat(m.arg<float>(0), 0, 1, 0, 16));
                int y = 16-(mapfloat(m.arg<float>(1), 0, 1, 0, 16))+1; //flip asse y, dato che l'origine del display e della matrice sono diversi
                Serial.print("x: "); Serial.print(x);  Serial.println("");
                Serial.print("y: "); Serial.print(y);  Serial.println("");
                if (x != -16 && y != -16) {
                        leds[XY(x,y)] = CRGB::HotPink;
                }
                else {
                        fill_solid( leds, NUM_LEDS, CRGB::Black);
                }
                FastLED.show();
        });

        //E131 setup unicast
        //if (e131.begin(E131_MULTICAST, UNIVERSE, UNIVERSE_COUNT))
        if (e131.begin(E131_UNICAST)) {
                Serial.println(F("Listening for e131 data..."));
        }
        else {
                Serial.println(F("*** e131.begin failed ***"));
        }

        //setup FASTLED
        FastLED.addLeds<SK9822, 23, 18, BGR>(leds, NUM_LEDS).setCorrection(TypicalSMD5050);
        FastLED.setMaxPowerInMilliWatts(30000); //power limit draw
}

void callbackMQTT(char* topic, byte* message, unsigned int length) { //callback MQTT
        Serial.print("Message arrived on topic: ");
        Serial.print(topic);
        Serial.print(". Message: ");
        String messageTemp;

        for (int i = 0; i < length; i++) {
                Serial.print((char)message[i]);
                messageTemp += (char)message[i];
        }

        String r =  getValue(messageTemp, ',', 0);
        String g =  getValue(messageTemp, ',', 1);
        String b =  getValue(messageTemp, ',', 2);
        Serial.printf("Received R: %s G: %s B: %s", r.c_str(), g.c_str(), b.c_str());
        Serial.println();

        fill_solid( leds, NUM_LEDS, CRGB(r.toInt(), g.toInt(), b.toInt()));
        FastLED.show();

        /*if (String(topic) == "esp32/index") {
            //fai qualcosa
           }*/
}

void reconnect() {
        // Loop until we're reconnected
        while (!client.connected()) {
                Serial.print("Attempting MQTT connection...");
                // Attempt to connect
                if (client.connect("ESP32Client")) {
                        Serial.println("connected");
                        // Subscribe
                        client.subscribe("interactive-LED-matrix/esp32/+");
                } else {
                        Serial.print("failed, rc=");
                        Serial.print(client.state());
                        Serial.println(" try again in 5 seconds");
                        // Wait 5 seconds before retrying
                        delay(5000);
                }
        }
}

String getValue(String data, char separator, int index) { //split della stringa MQTT, preso da https://github.com/awilhelmer/esp8266-fastled-mqtt/blob/master/esp8266-fastled-mqtt.ino#L368
        int found = 0;
        int strIndex[] = { 0, -1 };
        int maxIndex = data.length() - 1;

        for (int i = 0; i <= maxIndex && found <= index; i++) {
                if (data.charAt(i) == separator || i == maxIndex) {
                        found++;
                        strIndex[0] = strIndex[1] + 1;
                        strIndex[1] = (i == maxIndex) ? i + 1 : i;
                }
        }
        return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

int mapfloat(float x, float in_min, float in_max, float out_min, float out_max) {
        return (int) ((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min);
}

int XY( int x, int y) { //calcolo LED index
        int i;
        if( y & 0x01) {//se il bit meno significativo è settato allora la riga è dispari e va al contrario
                int reverseX = (17 - 1) - x;
                i = (y * 17) + reverseX;
        } else {
                // altrimenti riga pari
                i = (y * 17) + x;
        }
        return i;
}

void loop() {
        if (!client.connected()) {
                reconnect();
        }
        client.loop();

        osc.parse();

        if (!e131.isEmpty()) {
                e131_packet_t packet;
                e131.pull(&packet); // Pull packet from ring buffer
                int multipacketOffset = (htons(packet.universe) - UNIVERSE)*170; //Calcolo offset universo/canali. Se ci son più di 512 canali (limite del protocollo E131; 510 nel nostro caso; 3 canali x LED = 170 led massimi per universo), il client manda l'universo successivo con altri 512 canali.
                int len = (170 + multipacketOffset > NUM_LEDS) ? (NUM_LEDS - multipacketOffset) * 3 : 510;
                if (NUM_LEDS <= multipacketOffset) return;

                memcpy(leds + multipacketOffset, packet.property_values + 1, len); //Motivo per qui utilizzo le API di FASTLED nonostante su ESP32, utilizzi il bitbanging per comandare strisce LED SPI.
                //Posso copiare la porzione di memoria del pacchetto e131 direttamente nell'array dei LED senza dover usare un ciclo per rileggere tutti i singol canali e assegnarli all'array dei LED.
                /*
                   Altrimenti sarebbe così:
                   for (int i = 0; i < len; i++) {
                   int j = i * 3 + 1;
                   leds[i + multipacketOffset] = CRGB(packet.property_values[j], packet.property_values[j + 1], packet.property_values[j + 2]);
                   }
                 */

                FastLED.show();

                /*Serial.printf("Universe %u / %u Channels | Packet#: %u / Errors: %u / CH1: %u\n",
                              htons(packet.universe),  // The Universe for this packet
                              htons(packet.property_value_count) - 1, // Start code is ignored, we're interested in dimmer data
                              e131.stats.num_packets,  // Packet counter
                              e131.stats.packet_errors, // Packet error counter
                              packet.property_values[1]); // Dimmer data for Channel 1 (first color for led n° 1 in that universe)
                 */

        }
}
