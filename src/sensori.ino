#include <FastLED.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define TRIG_PIN 18
#define ECHO_PIN_X 5
#define ECHO_PIN_Y 17
#define BTN_PIN 2
#define BAUD_RATE 115200

#define NUM_LEDS 289

TaskHandle_t Task1;
Adafruit_SSD1306 display(128, 32, &Wire, 4);

int x_axis=0;
int y_axis=0;
unsigned long durationy = 0;
int distancey=0;

CRGB leds[NUM_LEDS];

void setup() {
        Serial.begin(BAUD_RATE);

        pinMode(TRIG_PIN, OUTPUT);
        pinMode(ECHO_PIN_X, INPUT);
        pinMode(ECHO_PIN_Y, INPUT);
        pinMode(BTN_PIN, INPUT);

        display.begin(SSD1306_SWITCHCAPVCC, 0x3C); // Address 0x3C for 128x32 OLED
        display.clearDisplay();
        display.display();

        xTaskCreatePinnedToCore(
                Task1code,       /* Task function. */
                "Task1",         /* name of task. */
                10000,           /* Stack size of task */
                NULL,            /* parameter of the task */
                1,               /* priority of the task */
                &Task1,          /* Task handle to keep track of created task */
                0);              /* pin task to core 0 */
        delay(500); //delay per inizializzare core
        FastLED.addLeds<SK9822, 23, 18, BGR>(leds, NUM_LEDS).setCorrection(TypicalSMD5050);
        FastLED.setMaxPowerInMilliWatts(30000);

        Serial.print("setup() running on core ");
        Serial.println(xPortGetCoreID());
}

int XY( int x, int y) { //calcolo index LED
        int i;
        if( y & 0x01) {//se il bit meno significativo è settato allora la riga è dispari e va al contrario
                int reverseX = (17 - 1) - x;
                i = (y * 17) + reverseX;
        } else {
                // altrimenti riga pari
                i = (y * 17) + x;
        }
        return i;
}

void Task1code( void * pvParameters ){ //callback per Task1, calcolo e map valore sensore su asse y
        for(;;) {
                Serial.print("Task1 running on core ");
                Serial.println(xPortGetCoreID());

                durationy= pulseIn(ECHO_PIN_Y, HIGH);
                distancey= (durationy/2) / 29.1;
                if (distancey<=20) {
                        y_axis = map(distancey, 0, 20, 0, 16);
                }

                if(durationy==0) {
                        Serial.println("Warning: no pulse from sensor y");
                        delay(10);
                }

                delay(1000);
        }
}

void loop() {
        //OLED setup
        display.clearDisplay();
        display.setTextSize(1);
        display.setTextColor(WHITE);
        display.setCursor(0,0);

        //trigger cycle
        digitalWrite(TRIG_PIN, LOW);
        delayMicroseconds(2);
        digitalWrite(TRIG_PIN, HIGH);
        delayMicroseconds(10);
        digitalWrite(TRIG_PIN, LOW);
        Serial.print("loop() running on core ");
        Serial.println(xPortGetCoreID());
        int btnState = digitalRead(BTN_PIN);

//calcolo e map valore sensore su asse x
        unsigned long duration= pulseIn(ECHO_PIN_X, HIGH);
        int distance= (duration/2) / 29.1;
        if (distance<=20) {
                x_axis = map(distance, 0, 20, 0, 16);
        }

        //controllo finale e display su matrice
        if(duration==0) {
                Serial.println("Warning: no pulse from sensor x");
                delay(10);
        }
        if(duration!=0 && durationy != 0) {
                leds[XY(x_axis, y_axis)] = CRGB::HotPink;
                //stats su OLED
                display.printf("asse x: %d;\nasse Y: %d\n", x_axis, y_axis);
                display.printf("cm X: %d; cm Y: %d\n", distance, distancey);
                display.printf("index LED: %d", XY(x_axis, y_axis));
                display.display();
                //bottone per il reset della matrice
                if (btnState == HIGH) {
                        fill_solid( leds, NUM_LEDS, CRGB::Black);
                }
                FastLED.show();
        }
        delay(999);
}
