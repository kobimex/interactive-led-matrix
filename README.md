# Interactive LED matrix

*Autore*: Mirko Milovanovic

*Descrizione*: Creazione di una matrice LED 17x17 controllata da un ESP32.
La matrice è (semi)interattiva, potendo essere controllata in vari modi: attraverso il protocollo E131 è possibile inviare animazioni e giochi di luce da un controller
esterno in modo wireless; creando un sensore "touch" utilizzando 2 sensori ad ultrasuoni per gli assi X e Y è possibile interagire con un minimo di UI; via MQTT con settaggio di colore via valori RGB; via OSC, disegnando sulla matrice utilizzanfo lo scheermo del cellulare.

*Data di presentazione*: 31 luglio '19

## Hardware* (provvisorio):

* ESP32
* 2x ultrasonic sensor HY-SRF05
* SK9822 RGB LED strip
* Trasformatore 5V, 100W, 20A

## Schema elettrico (provvisorio):
![](schema_bb.png)

## Immagini (non complete)
![](C4569540-B30D-4A00-B760-9CBF5C1B567C_1_105_c.jpeg)
